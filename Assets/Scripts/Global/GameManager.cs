using System;
using UnityEngine;
using UnityEngine.SceneManagement;

[DefaultExecutionOrder(-2)]
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get { return _instance; } }
    private static GameManager _instance;

    public event Action OnUpdate;
    public event Action OnFixedUpdate;
    public event Action OnGameStarted;
    public event Action<int> OnScoreChanged;

    public int Score 
    { 
        get
        {
            return _score;
        }
        
        set
        {
            _score = value;

            OnScoreChanged?.Invoke(value);
        }
    }

    private int _score;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        _score = 0;
    }

    private void Update()
    {
        OnUpdate?.Invoke();
    }

    private void FixedUpdate()
    {
        OnFixedUpdate?.Invoke();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        OnGameStarted?.Invoke();
    }
}
