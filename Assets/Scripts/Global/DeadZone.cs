using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    private BoxCollider2D _deadZoneCollider;
    private Bounds _deadZoneBounds;

    private void Awake() 
    {
        _deadZoneCollider = GetComponent<BoxCollider2D>();
        _deadZoneBounds = CameraInfo.CameraBounds;

        float xColliderSize = _deadZoneBounds.MaxX * 2f + 2f;
        float yColliderSize = _deadZoneBounds.MaxY * 2f + 5f;

        _deadZoneCollider.size = new Vector2(xColliderSize, yColliderSize);
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if(other.gameObject.TryGetComponent(out Bullet bullet))
        {
            bullet.ReturnToPool();
        }

        if(other.gameObject.TryGetComponent(out Enemy enemy))
        {
            enemy.ShutDown();
            enemy.ReturnToPool();
        }

        if(other.gameObject.TryGetComponent(out IBuff buff))
        {
            buff.ShutDown();
        }
    }
}
