using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, IActivable, IInActivable
{
    [SerializeField] private List<EnemyType> _enemiesTypes;
    [SerializeField] private float _spawnRate;

    private Bounds _spawnBounds;
    private IEnumerator _spawn;
    private Vector2 _positionToSpawn;
    private int _randomEnemy;

    private void Awake() 
    {
        _spawnBounds = new Bounds(CameraInfo.CameraBounds.MinX,
                                  CameraInfo.CameraBounds.MaxX,
                                  CameraInfo.CameraBounds.MaxY,
                                  CameraInfo.CameraBounds.MaxY + 1f);

        _spawn = SpawnEnemy();

        _positionToSpawn = transform.position;
    }

    private void OnEnable() 
    {
        GameManager.Instance.OnGameStarted += OnGameStarted;
        Player.OnGameOver += OnGameOver;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnGameStarted -= OnGameStarted;
        Player.OnGameOver -= OnGameOver;
    }

    private void OnGameStarted()
    {
        Launch();
    }

    private void OnGameOver()
    {
        ShutDown();
    }

    private IEnumerator SpawnEnemy()
    {
        WaitForSeconds pause = new WaitForSeconds(_spawnRate);

        while(true)
        {
            _positionToSpawn.x = Random.Range(_spawnBounds.MinX, _spawnBounds.MaxX);
            _positionToSpawn.y = Random.Range(_spawnBounds.MinY, _spawnBounds.MaxY);

            _randomEnemy = Random.Range(0, _enemiesTypes.Count);

            GameObject enemyPrefab = EnemyPooler.Instance.GetObjectFromPool(_enemiesTypes[_randomEnemy], _positionToSpawn, transform.rotation);

            IActivable enemyActivation = enemyPrefab.GetComponent<IActivable>();
            enemyActivation.Launch();

            yield return pause;
        }
    }

    public void Launch()
    {
        StartCoroutine(_spawn);
    }

    public void ShutDown()
    {
        StopAllCoroutines();
    }
}
