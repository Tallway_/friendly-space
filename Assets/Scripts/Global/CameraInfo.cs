using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class CameraInfo : MonoBehaviour
{
    public Camera MainCamera { get; private set;}
    public static Bounds CameraBounds { get; private set; }

    private void Awake() 
    {
        MainCamera = Camera.main;

        Vector3 maxCameraValue = MainCamera.ViewportToWorldPoint(new Vector3(1, 1, MainCamera.nearClipPlane));
        Vector3 minCameraValue = MainCamera.ViewportToWorldPoint(new Vector3(0, 0, MainCamera.nearClipPlane));

        CameraBounds = new Bounds(minCameraValue.x, maxCameraValue.x, minCameraValue.y, maxCameraValue.y);
    }
}
