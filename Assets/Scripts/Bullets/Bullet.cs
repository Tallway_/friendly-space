using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int _damageValue; 
    [SerializeField] private float _speed;
    [SerializeField] private BulletType _bulletType;

    public GameInstances Owner { get; private set; }
    public int DamageValue { get { return _damageValue; } }

    private Vector2 _directionOfVelocity;
    private Rigidbody2D _rigidbody;


    private void Awake() 
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Init(GameInstances owner) 
    {
        Owner = owner;

        SetVelocity();
    }

    public void ReturnToPool()
    {
        BulletPooler.Instance.ReturnObjectToPool(this.gameObject, _bulletType);
    }

    private void SetVelocity()
    {
        _rigidbody.velocity = transform.up * _speed;
    }
}
