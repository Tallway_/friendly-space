using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Turret : MonoBehaviour
{
    [SerializeField] protected float _shootingRate;
    [SerializeField] protected List<Socket> _bulletSockets;
    [SerializeField] protected TurretType _turretType;

    protected SpriteRenderer _turretRenderer;
    protected IEnumerator _shooting;

    public TurretType TurretType { get { return _turretType; } }
    

    protected void Init()
    {
        _turretRenderer = GetComponent<SpriteRenderer>();

        _shooting = Shoot();
    }

    public void Activate()
    {
        _turretRenderer.enabled = true;
        StartCoroutine(_shooting);
    }

    public void Disactivate()
    {
        _turretRenderer.enabled = false;
        StopCoroutine(_shooting);
    }

    protected IEnumerator Shoot()
    {
        WaitForSeconds pause = new WaitForSeconds(_shootingRate);

        while(true)
        {
            foreach(Socket socket in _bulletSockets)
            {
                Transform socketTransform = socket.gameObject.transform;

                GameObject bulletPrefab = BulletPooler.Instance.GetObjectFromPool(socket.BulletType, socketTransform.position, socketTransform.rotation);

                Bullet bullet = bulletPrefab.GetComponent<Bullet>();
                bullet.Init(GameInstances.Player);
            }

            yield return pause;
        }
    }
}
