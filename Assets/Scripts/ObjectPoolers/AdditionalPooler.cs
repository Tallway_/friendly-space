using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalPooler : ObjectPooler<AdditionalPooler>
{
    public static AdditionalPooler Instance { get { return _instance; } }
    private static AdditionalPooler _instance;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        Init();
    }
}
