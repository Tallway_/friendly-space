using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : ObjectPooler<BulletType> 
{
    public static BulletPooler Instance { get { return _instance; } }
    private static BulletPooler _instance;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        Init();
    }
}
