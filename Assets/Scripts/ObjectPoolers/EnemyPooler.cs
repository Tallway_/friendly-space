using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPooler : ObjectPooler<EnemyType> 
{
    public static EnemyPooler Instance { get { return _instance; } }
    private static EnemyPooler _instance;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        Init();
    }
}
