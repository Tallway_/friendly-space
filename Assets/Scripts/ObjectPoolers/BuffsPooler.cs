using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffsPooler : ObjectPooler<BuffsType> 
{
    public static BuffsPooler Instance { get { return _instance; } }
    private static BuffsPooler _instance;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        Init();
    }
}
