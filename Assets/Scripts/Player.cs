using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private ShipController Ship;
    
    public static event Action OnGameOver;

    private ShipController _ship;

    private void Awake() 
    {
        _ship = Instantiate(Ship, transform.position, Quaternion.identity, transform);
        _ship.OnDead += OnDead;
    }

    private void OnEnable() 
    {
        //GameManager.Instance.OnUpdate += OnUpdate;
        //GameManager.Instance.OnFixedUpdate += OnFixedUpdate;
        
    }

    private void OnDisable() 
    {
        //GameManager.Instance.OnUpdate -= OnUpdate;
       // GameManager.Instance.OnFixedUpdate -= OnFixedUpdate;
       _ship.OnDead -= OnDead;
    }

    private void OnDead()
    {
        OnGameOver?.Invoke();
    }
}
