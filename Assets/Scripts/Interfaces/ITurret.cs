using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITurret 
{
    TurretType TurretType { get; }
    void Activate();
    void Disactivate();
}
