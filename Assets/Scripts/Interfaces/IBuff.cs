using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBuff
{
    BuffsType BuffsType { get; }
    void Launch();
    void ShutDown();
}
