public interface IActivable
{
    void Launch();
}