using System;
using TMPro;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _healthText;

    private void OnEnable() 
    {
        ShipController.OnHealthChanged += (value) => OnHealthChanged(value);
    }

    private void OnDisable() 
    {
        ShipController.OnHealthChanged -= OnHealthChanged;
    }

    private void OnHealthChanged(int changedValue)
    {
        _healthText.text = changedValue.ToString();
    }
}
