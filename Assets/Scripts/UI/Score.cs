using System;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText;

    private void OnEnable() 
    {
        GameManager.Instance.OnScoreChanged += (value) => OnScoreChanged(value);
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnScoreChanged -= OnScoreChanged;
    }

    private void OnScoreChanged(int changedValue)
    {
        _scoreText.text = changedValue.ToString();
    }
}
