using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPageController : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private const string GameStarted = nameof(GameStarted);
    private const string GameOver = nameof(GameOver);

    private void OnEnable() 
    {
        GameManager.Instance.OnGameStarted += OnGameStarted;
        Player.OnGameOver += OnGameOver;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnGameStarted -= OnGameStarted;
        Player.OnGameOver -= OnGameOver;
    }

    private void OnGameStarted()
    {
        _animator.SetTrigger(GameStarted);
    }

    private void OnGameOver()
    {
        _animator.SetTrigger(GameOver);
    }
}
