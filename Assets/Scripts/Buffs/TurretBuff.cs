using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class TurretBuff : MonoBehaviour, IBuff
{
    [SerializeField] private TurretType _turretType;
    [SerializeField] private BuffsType _buffType;
    [SerializeField] private float _speed;

    public BuffsType BuffsType { get { return _buffType; } }

    private Rigidbody2D _rigidbody;


    private void Awake() 
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Launch()
    {
        _rigidbody.velocity = Vector2.down * _speed;
    }

    public void ShutDown()
    {
        BuffsPooler.Instance.ReturnObjectToPool(this.gameObject, _buffType);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.TryGetComponent(out ShipController _ship))
        {
            _ship.ChangeTurretTo(_turretType);
            ShutDown();
        }       
    }
}
