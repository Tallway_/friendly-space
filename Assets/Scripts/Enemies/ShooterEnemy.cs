using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemy : Enemy, IActivable, IInActivable, IDamagable
{
    private IEnumerator _movement;
    private Bounds _movementBounds;
    private readonly float _movementTime = 0.5f;

    private Vector2 _positionToMove;

    private void Awake() 
    {
        ParentInit();

        _movement = Move();

        _movementBounds = new Bounds(CameraInfo.CameraBounds.MinX,
                                     CameraInfo.CameraBounds.MaxX,
                                     0f,
                                     CameraInfo.CameraBounds.MaxY);
    }

    private IEnumerator Move()
    {
        while(true)
        {
            _positionToMove.x = Random.Range(_movementBounds.MinX, _movementBounds.MaxX);
            _positionToMove.y = Random.Range(_movementBounds.MinY, _movementBounds.MaxY);

            yield return MoveToPoint(_positionToMove);
        }
    }

    private IEnumerator MoveToPoint(Vector2 endPosition)
    {
        Vector2 startPosition = transform.position;

        for(float i = 0; i < 1; i += Time.deltaTime / _movementTime)
        {
            transform.position = Vector2.Lerp(startPosition, endPosition, i);

            yield return null;
        }
    }

    public void Launch()
    {
        StartCoroutine(_movement);
        StartCoroutine(_shooting);
    }
}
