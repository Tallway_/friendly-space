
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected EnemyType _enemyType;
    [Space, SerializeField] protected List<Socket> _bulletSockets;
    [Space, SerializeField] protected List<GameObject> _buffsPrefab;
    [Space, SerializeField] protected int _shipHealth;
    [SerializeField] protected int _scorePoints;
    [SerializeField] protected float _chanceToSpawn;
    [SerializeField] protected float _shootingRate;

    protected int _health;
    protected IEnumerator _shooting;
    protected List<IBuff> _buffsInterface;

    public EnemyType EnemyType { get { return _enemyType; } }
    public float ScorePoints { get { return _scorePoints; } }
    public int Health 
    { 
        get 
        { 
            return _health; 
        } 

        set
        {
            if(value <= 0)
            {
                _health = _shipHealth;

                GameManager.Instance.Score += _scorePoints;

                TrySpawmBuff();
                ReturnToPool();
            }
            else
            {
                _health = value;
            }
        }
    }


    protected void ParentInit() 
    {
        _buffsInterface = new List<IBuff>();

        foreach(GameObject prefab in _buffsPrefab)
        {
            IBuff buffInterface = prefab.GetComponent<IBuff>();
            _buffsInterface.Add(buffInterface);
        }

        _health = _shipHealth;
        _shooting = Shoot();
    }

    protected void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.TryGetComponent(out Bullet bullet))
        {
            if(bullet.Owner == GameInstances.Player)
            {
                ApplyDamage(bullet.DamageValue);
            }

            bullet.ReturnToPool();
        }
    }

    protected IEnumerator Shoot()
    {
        WaitForSeconds pause = new WaitForSeconds(_shootingRate);

        while(true)
        {
            foreach(Socket socket in _bulletSockets)
            {
                Transform socketTransform = socket.gameObject.transform;

                GameObject bulletPrefab = BulletPooler.Instance.GetObjectFromPool(socket.BulletType, socketTransform.position, socketTransform.rotation);

                Bullet bullet = bulletPrefab.GetComponent<Bullet>();
                bullet.Init(GameInstances.Enemy);
            }

            yield return pause;
        }
    }

    protected void TrySpawmBuff()
    {
        float randomChance = Random.Range(0f, 1f);

        if(_chanceToSpawn >= randomChance)
        {
            int randomBuff = Random.Range(0, _buffsInterface.Count);

            GameObject buffObject = BuffsPooler.Instance.GetObjectFromPool(_buffsInterface[randomBuff].BuffsType, transform.position, Quaternion.identity);
            IBuff buffInterface = buffObject.GetComponent<IBuff>();

            buffInterface.Launch();
        }
    }

    public void ReturnToPool()
    {
        EnemyPooler.Instance.ReturnObjectToPool(this.gameObject, _enemyType);
    }

    public void ApplyDamage(int value)
    {
        Health -= value;
    }

    public void ShutDown()
    {
        StopAllCoroutines();
    }

}
