using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SimpleEnemy : Enemy, IActivable, IInActivable, IDamagable
{
    [SerializeField] private float _speed;

    private Rigidbody2D _rigidbody;

    private void Awake() 
    {
        _rigidbody = GetComponent<Rigidbody2D>();

        ParentInit();      
    }


    private void Move()
    {
        _rigidbody.velocity = Vector2.down * _speed;
    }

    public void Launch()
    {
        Move();
        StartCoroutine(_shooting);
    }
}
