using System;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour, IDamagable
{
    [SerializeField] private List<GameObject> _turretObjects;
    [Space, SerializeField] private float _shipSpeed;
    [SerializeField] private int _shipHealth;
    [SerializeField] private int _collisionDamage;

    public event Action OnDead;
    public static event Action<int> OnHealthChanged;

    private List<ITurret> _shipTurrets;
    private ITurret _turret;
    private Rigidbody2D _rigidbody;
    private Bounds _bounds;
    private Vector3 _accelerationValue; 
    private int _health;
    private readonly float _expandValue = 2f;

    public int Health 
    { 
        get 
        { 
            return _health; 
        } 

        set
        {
            if(value <= 0)
            {
                _health = 0;
                
                OnHealthChanged?.Invoke(_health);

                OnDead?.Invoke();

                Destroy(gameObject);
            }
            else if(value > _shipHealth)
            {
                _health = _shipHealth;
            }
            else 
            {
                _health = value;
            }
        }
    }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();

        _bounds = new Bounds(CameraInfo.CameraBounds.MinX,
                             CameraInfo.CameraBounds.MaxX, 
                             CameraInfo.CameraBounds.MinY,
                             CameraInfo.CameraBounds.MinY + _expandValue);
        
       _shipTurrets = new List<ITurret>();
       _health = _shipHealth;
    }

    private void OnEnable() 
    {
        GameManager.Instance.OnUpdate += OnUpdate;
        GameManager.Instance.OnFixedUpdate += OnFixedUpdate;
        GameManager.Instance.OnGameStarted += OnGameStarted;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnUpdate -= OnUpdate;
        GameManager.Instance.OnFixedUpdate -= OnFixedUpdate;
        GameManager.Instance.OnGameStarted -= OnGameStarted;
    }

    private void OnGameStarted()
    {
        _turret.Activate();
    }

    private void OnUpdate()
    {
        Vector3 clampedPosition = transform.position;

        clampedPosition.x = Mathf.Clamp(clampedPosition.x, _bounds.MinX, _bounds.MaxX);
        clampedPosition.y = Mathf.Clamp(clampedPosition.y, _bounds.MinY, _bounds.MaxY);

        transform.position = clampedPosition;
    }

    private void OnFixedUpdate()
    {
        _accelerationValue = Input.acceleration;

        Vector2 playerVelocity = new Vector2(_accelerationValue.x, -_accelerationValue.z - 0.5f);

        _rigidbody.velocity = playerVelocity * _shipSpeed;
    }

    public void ChangeTurretTo(TurretType turretType)
    {
        _turret.Disactivate();

        _turret = _shipTurrets.Find(turret => turret.TurretType == turretType);

        _turret.Activate();
    }

    private void Start() 
    {
        foreach(GameObject turretObject in _turretObjects)
        {
            ITurret turret = turretObject.GetComponent<ITurret>();
            turret.Disactivate();

            _shipTurrets.Add(turret);
        }  

        _turret = _shipTurrets.Find(turret => turret.TurretType == TurretType.SimpleTurret);         
    }

    public void ApplyDamage(int value)
    {
        Health -= value;

        OnHealthChanged?.Invoke(Health);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.TryGetComponent(out Bullet bullet))
        {
            if(bullet.Owner == GameInstances.Enemy)
            {
                ApplyDamage(bullet.DamageValue);
            }

            bullet.ReturnToPool();
        }

        if(other.TryGetComponent(out Enemy enemy))
        {
            enemy.ShutDown();
            enemy.ReturnToPool();

            ApplyDamage(_collisionDamage);
        }
    }
}
